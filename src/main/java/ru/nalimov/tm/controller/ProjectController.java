package ru.nalimov.tm.controller;

import ru.nalimov.tm.entity.Project;
import ru.nalimov.tm.entity.User;
import ru.nalimov.tm.enumerated.Role;
import ru.nalimov.tm.service.ProjectService;
import ru.nalimov.tm.service.UserService;

import java.util.List;

public class ProjectController extends AbstractController {

    private final ProjectService projectService;
    private final UserService userService;

    public ProjectController(ProjectService projectService, UserService userService) {
        this.projectService = projectService;
        this.userService = userService;
    }


    public int addProjectToUser() {
        System.out.println("ADDING PROJECT TO USER");
        try {
            System.out.print("Enter project id: ");
            final Long projectId = Long.parseLong(scanner.nextLine());
            System.out.print("Enter user id: ");
            final Long userId = Long.parseLong(scanner.nextLine());
            final Project project = projectService.addProjectToUser(projectId, userId);
            System.out.println("Added user to project: " + project.toString());
        } catch (final Exception e) {
            final String command = printError();
            if (command.equals("cancel")) {
                return 0;
            }
            if (command.equals("yes"))
                addProjectToUser();
        }
        return 0;
    }

    public int addProjectToUser(final Long userId) {
        if (userId == null)
            return addProjectToUser();
        System.out.println("ADDING PROJECT TO USER");
        try {
            System.out.print("Enter project id: ");
            final Long projectId = Long.parseLong(scanner.nextLine());
            final Project project = projectService.addProjectToUser(projectId, userId);
            System.out.println("Added user to project: " + project.toString());
        } catch (final Exception e) {
            final String command = printError();
            if (command.equals("cancel")) {
                return 0;
            }
            if (command.equals("yes"))
                addProjectToUser(userId);
        }
        return 0;
    }

    public int createProject() {
        System.out.println("[CREATE_PROJECT]");
        System.out.println("INPUT PROJECT NAME");
        final String name = scanner.nextLine();
        System.out.println("INPUT PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        projectService.create(name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int updateProjectByIndex() {
        System.out.println("[UPDATE_PROJECT]");
        System.out.println("ENTER, PROJECT INDEX:");
        final int index = Integer.parseInt(scanner.nextLine()) -1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("INPUT PROJECT NAME");
        final String name = scanner.nextLine();
        System.out.println("INPUT PROJECT DESCRIPTION:");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }

    public int updateProjectById() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("INPUT PROJECT ID");
        final long id = Long.parseLong(scanner.nextLine());
        final Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return 0;
        }
        System.out.println("PLEASE, ENTER PROJECT NAME");
        final String name = scanner.nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION");
        final String description = scanner.nextLine();
        projectService.update(project.getId(), name, description);
        System.out.println("[OK]");
        return 0;
    }


    public int removeProjectByName() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = scanner.nextLine();
        final Project project = projectService.removeByName(name);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;

    }

    public int removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final long id = scanner.nextLong();
        final Project project = projectService.removeById(id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;

    }

    public int removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("PLEASE, ENTER PROJECT INDEX:");
        final long id = scanner.nextLong();
        final Project project = projectService.removeByIndex(id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
        return 0;

    }

    public int clearProject() {
        System.out.println("[CLEAR_PROJECT]");
        projectService.clear();
        System.out.println("[OK]");
        return 0;
    }

    public int listProject() {
        System.out.println("[LIST_PROJECT]");
        int index = 1;
        for (final Project project: projectService.findAll()) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        System.out.println("[OK]");
        return 0;
    }

    public void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }


    public int viewProjectByIndex() {
        System.out.println("ENTER, PROJECT INDEX:");
        final int index = scanner.nextInt() -1;
        final Project project = projectService.findByIndex(index);
        viewProject(project);
        return 0;
    }

    public int viewProjectById() {
        System.out.println("ENTER, PROJECT ID:");
        final long id = scanner.nextLong();
        final Project project = projectService.findById(id);
        viewProject(project);
        return 0;
    }

    public int clearProjectsByUserId() {
        System.out.println("CLEARING PROJECTS BY USERID");
        try {
            System.out.print("Enter user id: ");
            final Long userId = Long.parseLong(scanner.nextLine());
            projectService.clearProjectsByUserId(userId);
            System.out.println("Removed all projects with user id " + userId + " ");
        } catch (final Exception e) {
            final String command = printError();
            if (command.equals("cancel")) {
                return 0;
            }
            if (command.equals("yes"))
                clearProjectsByUserId();
        }
        return 0;
    }

    public int clearProjectsByUserId(final Long userId) {
        if (userId == null)
            return clearProjectsByUserId();
        System.out.println("CLEARING PROJECTS BY USERID");
        projectService.clearProjectsByUserId(userId);
        System.out.println(" Removed all projects with user id " + userId + " ");
        return 0;
    }

    public void viewProjects(final List<Project> projects) {
        int index = 1;
        projectService.ProjectSortByName(projects);
        for (Project project : projects) {
            System.out.println("Project №" + index + project.toString());
            index++;
        }
    }


    private Boolean checkPermission(final Long userId) {
        if (userId == null) {
            System.out.println("NO PERMISSIONS");
             return false;
        }
        final User user = userService.findById(userId);
        if (!user.getUserRole().equals(Role.ADMIN)) {
            System.out.println("NO PERMISSIONS");
            return false;
        }
        return true;
    }

    public int viewProjectsList(final Long userId) {
        System.out.println("VIEW PROJECTS");
        if (!checkPermission(userId))
            return 0;
        viewProjects(projectService.findAll());
        return 0;
    }

    public int viewProjectListByUserId(final Long userId) {
        System.out.println("VIEW PROJECTS FOR USER " + userId + " ");
        viewProjects(projectService.findAllByUserId(userId));
        return 0;
    }

}

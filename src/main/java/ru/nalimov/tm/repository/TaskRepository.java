package ru.nalimov.tm.repository;

import ru.nalimov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository {
    private List<Task> tasks = new ArrayList<>();

    public  Task create(final String name) {
        final Task project = new Task(name);
        tasks.add(project);
        return project;
    }

    public Task update(final Long id, final String name, final String description) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    public Task findByIndex(final int index) {
        if (index < 0 || index > tasks.size() -1) return null;
        return tasks.get(index);
    }

    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        for (final Task task: tasks ) {
            if (task.getName().equals(name)) return task;
        }
        return null;
    }

    public Task removeById(final Long id) {
        final Task task = findById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public Task removeByIndex(final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public Task findById(final Long id) {
        if (id == null) return null;
        for (final Task task: tasks ) {
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        if (id == null) return null;
        for (final Task task: tasks ) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public List<Task> findAddByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    public Task findByUserIdAndId(final Long id, final Long userId) {
        if (id == null)
            return null;
        for (final Task task : tasks) {
            final Long idUser = task.getUserId();
            if (idUser == null)
                continue;
            if (!idUser.equals(userId))
                continue;
            if (task.getId().equals(id))
                return task;
        }
        return null;
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> tasksWithProject = new ArrayList<>();
        for (final Task task : findAll()) {
            if (task.getProjectId() != null && task.getProjectId().equals(projectId))
                tasksWithProject.add(task);
        }
        return tasksWithProject;
    }

    public List<Task> findAllByUserId(final Long userId) {
        final List<Task> tasksWithUser = new ArrayList<>();
        for (final Task task : findAll()) {
            if (task.getUserId() != null && task.getUserId().equals(userId))
                tasksWithUser.add(task);
        }
        return tasksWithUser;
    }

    public void clearTasksByUserId(final Long userId) {
        final List<Task> tasks = findAllByUserId(userId);
        if (tasks.isEmpty())
            return;
        for (Task task : tasks) {
            removeById(task.getId());
        }
    }


    public void clear() {
        tasks.clear();
    }

    public List<Task> findAll() { return tasks; }

}

package ru.nalimov.tm.repository;

import ru.nalimov.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository {

    private List<Project> projects = new ArrayList<>();

    public  Project create(final String name) {
        final Project project = new Project(name);
        projects.add(project);
        return project;
    }

    public  Project create(final String name, final String description) {
        final Project project = new Project(name);
        project.setName(name);
        project.setDescription(description);
        projects.add(project);
        return project;
    }

    public Project update(final Long id, final String name, final String description) {
        final Project project = findById(id);
        if (project == null) return null;
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        projects.add(project);
        return project;
    }

    public Project findByIndex(int index) {
        if (index < 0 || index > projects.size() -1) return null;
        return projects.get(index);
    }

    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        for (final Project project: projects ) {
            if (project.getName().equals(name)) return project;
        }
        return null;
    }

    public Project findById(final Long id) {
        if (id == null) return null;
        for (final Project project: projects ) {
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    public Project removeById(final Long id) {
        final Project project = findById(id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    public Project removeByIndex(final Long id) {
        final Project task = findById(id);
        if (task == null) return null;
        projects.remove(task);
        return task;
    }

    public void clear() {
        projects.clear();
    }

    public List<Project> findAll() {
        return projects;
    }

    public void clearProjectsByUserId(final Long userId) {
        final List<Project> projects = findAllByUserId(userId);
        if (projects.isEmpty())
            return;
        for (Project project : projects){
            removeById(project.getId());
        }
    }

    public List<Project> findAllByUserId(final Long userId) {
        final List<Project> projectsWithUser = new ArrayList<>();
        for (final Project project : findAll()) {
            if (project.getUserId() != null && project.getUserId().equals(userId))
                projectsWithUser.add(project);
        }
        return projectsWithUser;
    }

    public Project createProject(final String name, final String description, final Long userId) {
        final Project project = new Project(name).description(description).userId(userId);
        projects.add(project);
        return project;
    }

    public Project findByUserIdAndId(final Long id, final Long userId) {
        if (id == null)
            return null;
        for (final Project project : projects) {
            final Long idUser = project.getUserId();
            if (idUser == null)
                continue;
            if (!idUser.equals(userId))
                continue;
            if (project.getId().equals(id))
                return project;
        }
        return null;
    }


}
